This mod adds carnivorous plants to the game. They spawn with an in-game event on random locations of the players map. 

Main features:
* New vore type exclusive to voracious plants
* Alternative graphics once the plants consume prey (1.4 only)
* AoE pheromone effect that causes a special mental state

The plants can be tamed, but the pheromones will still affect your pawns, they also lose their training very quickly, so don't bother.

Supported languages:
English
Japanese - Translator: https://twitter.com/Milcandybox