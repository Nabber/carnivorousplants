﻿using RimVore2;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace CarnivorousPlants
{
    public class Extension_AlternativeVoreGraphic : DefModExtension
    {
        GraphicData defaultAlternativeGraphic;
        Dictionary<BodyTypeDef, GraphicData> graphicsByBodyType = new Dictionary<BodyTypeDef, GraphicData>();

        public Graphic AlternativeGraphicFor(Pawn predator)
        {
            GraphicData alternativeGraphicData = null;
            VoreTrackerRecord record = predator.PawnData().VoreTracker.VoreTrackerRecords.FirstOrDefault();
            if(record != null)
            {
                BodyTypeDef bodyType = record.Prey?.story?.bodyType;
                if(bodyType != null)
                {
                    alternativeGraphicData = graphicsByBodyType.TryGetValue(bodyType);
                }
            }
            
            if(alternativeGraphicData == null)
            {
                alternativeGraphicData = defaultAlternativeGraphic;
            }

            return alternativeGraphicData.Graphic;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
                yield return error;
            if(defaultAlternativeGraphic == null)
                yield return $"Required field {nameof(defaultAlternativeGraphic)} must be set";
        }
    }

}
