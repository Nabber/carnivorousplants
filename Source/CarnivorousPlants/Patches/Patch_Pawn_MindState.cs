﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse.AI;

namespace CarnivorousPlants
{
    [HarmonyPatch(typeof(Pawn_MindState), "StartFleeingBecauseOfPawnAction")]
    internal class Patch_Pawn_MindState
    {
        [HarmonyPrefix]
        private static bool StopCarnivorousPlantsFromFleeing(Pawn_MindState __instance)
        {
            if (__instance.pawn.def.HasModExtension<Extension_CarnivorousPlantFlag>())
            {
                return false;
            }
            return true;
        }
    }
}
