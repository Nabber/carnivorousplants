﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace CarnivorousPlants
{
    /// <summary>
    ///  base game is extremely annoying about the pawn offsets. 
    ///  There seems to be a way to do it with base games "tweener" and "jitterer", 
    ///  but I have absolutely no idea what either of those mean, so I chose to implement my own lightweight solution
    /// </summary>
    [HarmonyPatch(typeof(PawnRenderer), "RenderPawnAt")]
    public static class Patch_PawnRenderer
    {
        [HarmonyPrefix]
        public static void ApplyDrawOffsetToPawns(Pawn ___pawn, ref Vector3 drawLoc, Rot4? rotOverride)
        {
            Extension_GlobalDrawOffset extension = ___pawn.def.GetModExtension<Extension_GlobalDrawOffset>();
            if(extension == null)
            {
                return;
            }
            Rot4 rotation = rotOverride ?? ___pawn.Rotation;
            Vector3 offset = extension.DrawOffsetForRot(rotation);
            drawLoc += offset;
        }
    }
}
