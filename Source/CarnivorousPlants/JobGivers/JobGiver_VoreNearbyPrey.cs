﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Verse;
//using Verse.AI;
//using RimVore2;

//namespace CarnivorousPlants
//{
//    public class JobGiver_VoreNearbyPrey : ThinkNode_JobGiver
//    {
//        protected override Job TryGiveJob(Pawn pawn)
//        {
//            Pawn target = CellTargetUtility.GetAllPawnsAtPosition(pawn, TargetLookVersion.EightWay)    // take all adjacent pawns
//                .Where(p => p.health?.hediffSet?.GetFirstHediffOfDef(Common.LureHediff)?.Severity > 0.5)    // limit to pawns that are lured fully
//                .RandomElementWithFallback();   // get a random pawn
//            if(target == null)
//            {
//                //Log.Warning("No vorable target");
//                return null;
//            }
//            VoreInteractionRequest request = new VoreInteractionRequest(pawn, target, VoreRole.Predator);
//            VoreInteraction interaction = VoreInteractionManager.Retrieve(request);
//            VorePathDef pathDef = interaction.ValidPaths.RandomElementWithFallback(null);
//            if(pathDef == null)
//            {
//                //Log.Warning("No vore path available");
//                return null;
//            }
//            VoreJob job = VoreJobMaker.MakeJob(Common.StationaryPredator, pawn, target);
//            job.VorePath = pathDef;
//            return job;
//        }
//    }
//}
