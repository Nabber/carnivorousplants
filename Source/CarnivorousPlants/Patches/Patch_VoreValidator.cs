﻿using HarmonyLib;
using RimVore2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace CarnivorousPlants
{
    [HarmonyPatch(typeof(VoreValidator), "CanParticipateInVore")]
    public static class Patch_VoreValidator
    {
        [HarmonyPostfix]
        private static void DisableAlreadyFullPlants(ref bool __result, Pawn pawn, ref string reason)
        {
            // do not fire if already disabled
            if(__result == false)
            {
                return;
            }
            if(pawn?.def?.HasModExtension<Extension_CarnivorousPlantFlag>() != true)
            {
                return;
            }
            VoreTracker tracker = pawn.PawnData()?.VoreTracker;
            if(tracker == null)
            {
                return;
            }
            if(!tracker.IsTrackingVore)
            {
                return;
            }

            reason = "NCP_PlantAlreadyFull".Translate();
            __result = false;
        }
    }
}
