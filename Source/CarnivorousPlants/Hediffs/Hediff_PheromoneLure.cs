﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace CarnivorousPlants
{
    public class Hediff_PheromoneLure : HediffWithComps
    {

        public override void Tick()
        {
            base.Tick();
            if (!base.pawn.IsHashIntervalTick(50))
            {
                return;
            }
            // up to 0.5 = 0%
            // at 1.0 = 10% chance
            float mentalStateChance = (base.Severity - 0.5f) * 0.2f;
            if (Rand.Chance(mentalStateChance))
            {
                pawn.mindState.mentalStateHandler.TryStartMentalState(Common.LureState);
            }
        }
    }
}
