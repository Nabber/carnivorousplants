﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimVore2;

namespace CarnivorousPlants
{
    public class IncidentWorker_Sprout : IncidentWorker
    {
        List<IntVec3> spawnCells = new List<IntVec3>();
        PawnKindDef chosenPlantKind;
        float plantPoints;

        protected override bool CanFireNowSub(IncidentParms parms)
        {
            if (!base.CanFireNowSub(parms))
            {
                return false;
            }
            // def only has target tag for player map, so target will always be of Map type
            Map map = (Map)parms.target;
            if (!TryPickPlantKind())
            {
                return false;
            }
            plantPoints = GetPlantPoints();
            int requiredCells = (int)Math.Ceiling(parms.points / plantPoints);
            //Log.Message("Required cells: " + requiredCells);
            if (!TryPopulateSpawnCells(map, requiredCells))
            {
                return false;
            }

            
            // the user could turn off fatal vore
            if (!RV2Mod.Settings.features.FatalVoreEnabled)
            {
                return false;
            }

            return true;
        }

        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            if (!CanFireNow(parms))
            {
                return false;
            }

            Map map = (Map)parms.target;

            // debug actions set a new point count, so we should make sure we have enough cells now
            int requiredCells = (int)Math.Ceiling(parms.points / plantPoints);
            if(spawnCells.Count != requiredCells)
            {
                if(!TryPopulateSpawnCells(map, requiredCells))
                {
                    return false;
                }
            }
            List<Thing> spawnedPlants = new List<Thing>();
            foreach(IntVec3 cell in spawnCells)
            {
                spawnedPlants.Add(GenSpawn.Spawn(PawnGenerator.GeneratePawn(chosenPlantKind), cell, map));
            }
            foreach(Thing plantThing in spawnedPlants)
            {
                Pawn plant = (Pawn)plantThing;
                Quirk removalQuirk = plant.QuirkManager().ActiveQuirks.Find(q => q.def == Common.EndoPredOnlyQuirk);
                if(removalQuirk != null)
                    plant.QuirkManager().RemovePersistentQuirk(removalQuirk);
            }
            SendStandardLetter(parms, new LookTargets(spawnedPlants), Array.Empty<NamedArgument>());
            return true;
        }

        private bool TryPopulateSpawnCells(Map map, int requiredCells, int cellAttempts = 10)
        {
            spawnCells.Clear();
            for (int i = 0; i < requiredCells * cellAttempts; i++)
            {
                if (CellFinder(map, out IntVec3 position))
                {
                    spawnCells.Add(position);
                    // if we found enough cells to spawn all plants, stop searching
                    if (spawnCells.Count >= requiredCells)
                    {
                        break;
                    }
                }
            }
            if(spawnCells.Count < requiredCells)
            {
                Log.Warning("IncidentWorker for spawning carnivorous plants could not find enough cells (" + requiredCells + ") in " + cellAttempts + " attempts, can't fire incident.");
                return false;
            }
            //Log.Message("found these valid cells: " + String.Join(", ", spawnCells));
            return true;
        }
        
        private bool CellFinder(Map map, out IntVec3 position)
        {
            position = map.AllCells.RandomElement();
            return CellValid(map, position);
        }

        private bool CellValid(Map map, IntVec3 pos)
        {
            // stay 10 tiles away from the map edge to prevent potential issues with the gas spawning!
            if(pos.CloseToEdge(map, 10))
            {
                return false;
            }
            if (!pos.Standable(map))
            {
                return false;
            }
            return map.terrainGrid.TerrainAt(pos).fertility >= 0.5;
        }

        private bool TryPickPlantKind()
        {
            IEnumerable<PawnKindDef> validRaces = DefDatabase<PawnKindDef>.AllDefsListForReading
                .Where(p => p.race != null
                    && p.race.HasModExtension<Extension_CarnivorousPlantFlag>());
            if (validRaces.EnumerableNullOrEmpty())
            {
                Log.Warning("IncidentWorker for spawning carnivorous plants could not find any valid pawn kind to spawn, can't fire incident.");
                return false;
            }
            chosenPlantKind = validRaces.RandomElement();
            return true;
        }

        private float GetPlantPoints()
        {
            Extension_CarnivorousPlantFlag plantFlag = chosenPlantKind?.race?.GetModExtension<Extension_CarnivorousPlantFlag>();
            if(plantFlag == null)
            {
                Log.Warning("Could not calculated points, no Extension_CarnivorousPlantFlag found for " + chosenPlantKind?.defName + " falling back to 200");
                return 200f;
            }
            return plantFlag.points;
        }
    }
}
