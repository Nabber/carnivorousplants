﻿#if !v1_4

//RIP in pieces, 1.5 introduced massive PawnRender changes, no way to salvage this that doesn't mean a complete rewrite of the code

#else


using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using HarmonyLib;
using RimVore2;
using RimWorld;
using UnityEngine;
using Verse;

namespace CarnivorousPlants.Patches
{
    /// <summary>
    /// This patch needs to do two things: 
    /// 1. Modify the HASH value that is used to determine the re-cache state of the materials
    /// 2. During caching the materials, if the current pawn is voring, replace the naked texture with the voring texture from the modExtensions
    /// </summary>
    [HarmonyPatch(typeof(PawnGraphicSet), "MatsBodyBaseAt")]
    internal class Patch_PawnGraphicSet
    {
        /// <summary>
        /// This takes care of the first job of this transpiler, getting the calculated hash value and modifying it with the information of whether a pawn is voring or not
        /// </summary>
        [HarmonyTranspiler]
        static IEnumerable<CodeInstruction> AddVoreStatusToMaterialHas(IEnumerable<CodeInstruction> instructions)
        {
            CodeInstruction insertionInstruction = instructions
                .First(c => c.opcode == OpCodes.Stloc_0);    // find the first place where the local variable "num" is saved

            foreach (CodeInstruction instruction in instructions)
            {
                if (instruction == insertionInstruction)
                {
                    yield return instruction;

                    yield return new CodeInstruction(OpCodes.Ldloc_0);  // load local variable "num"
                    yield return new CodeInstruction(OpCodes.Ldarg_0);  // load this
                    yield return new CodeInstruction(OpCodes.Ldfld, AccessTools.Field(typeof(PawnGraphicSet), "pawn")); // load .pawn
                    yield return new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(GlobalVoreTrackerUtility), nameof(GlobalVoreTrackerUtility.IsActivePredator))); // call static GlobalVoreTrackerUtility.IsTrackingVore(Pawn)
                    yield return new CodeInstruction(OpCodes.Add);  // add 0 (false) or 1 (true) to the hash
                    yield return new CodeInstruction(OpCodes.Stloc_0);  // save local variable "num"

                    //yield return new CodeInstruction(OpCodes.Ldloc_0);
                    //yield return new CodeInstruction(OpCodes.Ldarg_0);  // load this
                    //yield return new CodeInstruction(OpCodes.Ldfld, AccessTools.Field(typeof(PawnGraphicSet), "pawn"));
                    //yield return new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(Patch_PawnGraphicSet), "blaaa"));
                }
                else
                {
                    yield return instruction;
                }
            }
        }

        //static void blaaa(int num, Pawn pawn)
        //{
        //    RV2Log.Message($"pawn {pawn.LabelShort} currently has hash {num}", true);
        //}

        /// <summary>
        /// This takes care of replacing the naked texture with a vore texture in the cache so that the game draws the vore texture instead
        /// We are basically looking for the inside of the if(bodyCondition == RotDrawMode.Fresh) part -> remember that an enum is just an int, in this case with value 0, so the branching IL code is a brtrue!
        /// If the pawn is eligable for the vore texture, skip the original part of the method, otherwise run the original method
        /// Meaning we need both the ENTERING and the EXITING labels of the entire inner condition of the if
        /// 
        /// In the end we are basically adding an if(isEligable){} in which we put our code and MOVE the original method into the else{} part
        /// </summary>
        [HarmonyTranspiler]
        static IEnumerable<CodeInstruction> InterceptNakedTextureWithVoreTextureIfVoring(IEnumerable<CodeInstruction> instructions, ILGenerator il)
        {
            CodeInstruction originalStartInstruction;
            Label originalStartLabel;
            Label exitLabel;
            List<Label> stolenStartLabels;
            if (!FindAnchors())
            {
                Log.Warning("Could not transpile for Carnivorous Plants, one of the parameters was not found");
                foreach (CodeInstruction instruction in instructions)
                {
                    yield return instruction;
                }
                yield break;
            }

            foreach (CodeInstruction instruction in instructions)
            {
                if(instruction != originalStartInstruction)
                {
                    yield return instruction;
                }
                else
                {
                    CodeInstruction initialInstruction = new CodeInstruction(OpCodes.Ldarg_0); // load this
                    foreach(Label label in stolenStartLabels)
                    {
                        initialInstruction.labels.Add(label);
                    }
                    yield return initialInstruction;
                    yield return new CodeInstruction(OpCodes.Ldfld, AccessTools.Field(typeof(PawnGraphicSet), "pawn")); // load .pawn
                    yield return new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(Patch_PawnGraphicSet), "EligableForVoreGraphic"));   // call static EligableForVoreGraphic(Pawn)
                    yield return new CodeInstruction(OpCodes.Brfalse, originalStartLabel);  // defer to the original instructions if the pawn is not eligable for the texture swap

                    yield return new CodeInstruction(OpCodes.Ldarg_0);  // load this
                    yield return new CodeInstruction(OpCodes.Ldfld, AccessTools.Field(typeof(PawnGraphicSet), "cachedMatsBodyBase"));   // load the cache list to add to
                    yield return new CodeInstruction(OpCodes.Ldarg_0);  // load this
                    yield return new CodeInstruction(OpCodes.Ldfld, AccessTools.Field(typeof(PawnGraphicSet), "pawn")); // load .pawn
                    yield return new CodeInstruction(OpCodes.Ldarg_1);  // load method argument "facing"
                    yield return new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(Patch_PawnGraphicSet), "GetVoreGraphicMaterial")); // call static GetVoreGraphicMaterial(Pawn, Rot4)
                    yield return new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(List<Material>), "Add"));  // this will add our graphic to the cached list
                    yield return new CodeInstruction(OpCodes.Br, exitLabel);    // skip the original code

                    yield return instruction;   // yield the original ldarg_0, its labels have been stolen and attached to our ldarg_0, only the brfalse in our instruction chain can call this
                }
            }

            bool FindAnchors()
            {
                originalStartLabel = default(Label);
                exitLabel = default(Label);
                originalStartInstruction = null;

                int nakedFieldLoadIndex = instructions.FirstIndexOf(ci => ci.LoadsField(AccessTools.Field(typeof(PawnGraphicSet), "nakedGraphic")));

                CodeInstruction initialThisFieldLoadInstruction = instructions.ElementAt(nakedFieldLoadIndex - 3);
                Log.Message($"fieldload instruction: op: {initialThisFieldLoadInstruction.opcode} - operand {initialThisFieldLoadInstruction.operand}");

                // take the original labels
                stolenStartLabels = initialThisFieldLoadInstruction.ExtractLabels();

                // add a new label that only we call
                originalStartLabel = il.DefineLabel();
                initialThisFieldLoadInstruction.labels.Add(originalStartLabel);
                originalStartInstruction = initialThisFieldLoadInstruction;

                CodeInstruction finishingBreakInstruction = instructions.ElementAt(nakedFieldLoadIndex + 5);
                Log.Message($"exit instruction: op: {finishingBreakInstruction.opcode} - operand {finishingBreakInstruction.operand}");
                exitLabel = (Label)finishingBreakInstruction.operand;

                // 1.3 approach, maybe 1.4 approach will work for both
                //List<CodeInstruction> codeInstructions = instructions.ToList();
                //int startIndex = int.MaxValue;
                //for (int i = 0; i < codeInstructions.Count; i++)
                //{
                //    if(codeInstructions[i].opcode == OpCodes.Ldarg_2
                //        && codeInstructions[i + 1].opcode == OpCodes.Brtrue_S)
                //    {
                //        startIndex = i + 2;
                //    }
                //    if(i >= startIndex)
                //    {
                //        CodeInstruction instruction = codeInstructions[i];
                //        if(originalStartLabel == default(Label))
                //        {
                //            // if the start label has not been set yet, we are at the beginning of the original set of instructions, copy the very first one to use as an anchor for the yield return loop
                //            originalStartInstruction = instruction;
                //            // and then determine the start label to jump to in case we don't want to execute our own code
                //            originalStartLabel = instruction.labels.FirstOrFallback();
                //            // if instruction has no label, create it
                //            if(originalStartLabel == default(Label))
                //            {
                //                originalStartLabel = il.DefineLabel();
                //                instruction.labels.Add(originalStartLabel);
                //            }
                //        }
                //        // if the branch instruction is found, we have reached the end of the original instructions we want to move. Copy the targeted label for our own use and stop the collection of instructions
                //        if(instruction.opcode == OpCodes.Br_S)
                //        {
                //            exitLabel = (Label)instruction.operand;
                //            break;
                //        }
                //    }
                //}
                Log.Message($"origInst {originalStartInstruction} - startLab {originalStartLabel} - exitLab {exitLabel}");

                return originalStartInstruction != null 
                    && originalStartLabel != default(Label) 
                    && exitLabel != default(Label);
            }
        }
        public static bool EligableForVoreGraphic(Pawn pawn)
        {
            if (!pawn.def.HasModExtension<Extension_AlternativeVoreGraphic>())
            {
                return false;
            }
            return GlobalVoreTrackerUtility.IsActivePredator(pawn);
        }

        public static Material GetVoreGraphicMaterial(Pawn pawn, Rot4 facing)
        {
            if (!GlobalVoreTrackerUtility.IsActivePredator(pawn))
            {
                return null;
            }
            Extension_AlternativeVoreGraphic extension = pawn?.def?.GetModExtension<Extension_AlternativeVoreGraphic>();
            if (extension == null)
            {
                return null;
            }
            Graphic alternativeGraphic = extension?.AlternativeGraphicFor(pawn);
            if (alternativeGraphic == null)
            {
                return null;
            }
            //Log.Message($"Injecting new graphic {newGraphic.data.texPath}");
            return alternativeGraphic.MatAt(facing);
        }
    }
}

#endif