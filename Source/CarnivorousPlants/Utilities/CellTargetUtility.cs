﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace CarnivorousPlants
{
    public static class CellTargetUtility
    {
        public static IEnumerable<Pawn> GetAllPawnsAtPosition(Thing searcher, TargetLookVersion version = TargetLookVersion.EightWayAndInside)
        {
            if(searcher.MapHeld == null)
            {
                //Log.Message("Tried to retrieve positional pawns for null map on Thing " + searcher.Label);
                yield break;
            }
            IEnumerable<Thing> things = GetCells()?  // get cells based on targeting version
                .SelectMany(cell => cell.GetThingList(searcher.MapHeld));   // get all things that are on those cells
            if (things.EnumerableNullOrEmpty())
            {
                yield break;
            }

            foreach(Thing thing in things) 
            {
                if(thing != searcher && thing is Pawn cellPawn) // don't find yourself
                {
                    yield return cellPawn;  // we found a pawn
                }
            }

            IEnumerable<IntVec3> GetCells()
            {
                switch (version)
                {
                    case TargetLookVersion.EightWayAndInside:
                        return GenAdj.CellsAdjacent8WayAndInside(searcher);
                    case TargetLookVersion.EightWay:
                        return GenAdj.CellsAdjacent8Way(searcher);
                    case TargetLookVersion.Inside:
                        return GenAdj.CellsOccupiedBy(searcher);
                    case TargetLookVersion.Adjacent:
                        return GenAdj.CellsAdjacentCardinal(searcher);
                    default:
                        return new List<IntVec3>();
                }
            }
        }

        public static IEnumerable<Pawn> GetAllPawnsAt(Map map, IntVec3 cell)
        {
            return cell.GetThingList(map)
                .Where(thing => thing is Pawn)
                .Cast<Pawn>();
        }

        public static IEnumerable<IntVec3> GetAllInRadius(IntVec3 position, float radius)
        {
            // how I understand it is that base game has a massive array of offsets for a 0,0 cell, which acts as a growing spiral around 0,0
            // so first get the number of cells and then retrieve cells from the spiral until we have the full radius
            for (int i = 0; i < GenRadial.NumCellsInRadius(radius); i++)
            {
                yield return position + GenRadial.RadialPattern[i];
            }
        }
    }

    public enum TargetLookVersion
    {
        EightWayAndInside,
        EightWay,
        Inside,
        Adjacent
    }
}
