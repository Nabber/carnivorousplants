﻿using RimVore2;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace CarnivorousPlants
{
    public class MentalState_Lure : MentalState
    {
        public Pawn CurrentTarget;

        public override void MentalStateTick()
        {
            base.MentalStateTick();
            if (pawn.Map == null)
            {
                this.RecoverFromState();
                return;
            }
            bool needToAcquireTarget = CurrentTarget == null    // no target has been defined yet (just entered mental state)
                || !SpreaderVoreTargetUtility.SpreaderCanVoreTarget(pawn, CurrentTarget); // check target validity on interval
            
            if(needToAcquireTarget)
            {
                AcquireTarget();
            }
            if(pawn.CurJob?.def != VoreJobDefOf.RV2_VoreInitAsPrey && CurrentTarget != null)
            {
                var whitelist = new List<RV2DesignationDef>()
                {
                    RV2DesignationDefOf.fatal
                };
                VoreInteractionRequest request = new VoreInteractionRequest(pawn, CurrentTarget, VoreRole.Prey, designationWhitelist: whitelist);
                VoreInteraction interaction = VoreInteractionManager.Retrieve(request);
                if (!interaction.IsValid)
                {
                    // this should never happen, the PlantCanVoreTarget should have checked this
                    CurrentTarget = null;
                    return;
                }
                VorePathDef chosenPath = interaction.ValidPaths.RandomElementWithFallback();
                if(chosenPath == null)
                {
                    CurrentTarget = null;
                    return;
                }
                VoreJob job = VoreJobMaker.MakeJob(VoreJobDefOf.RV2_VoreInitAsPrey, base.pawn, CurrentTarget);
                job.VorePath = chosenPath;
                //Job job = JobMaker.MakeJob(Common.GotoLured, CurrentTarget);
                pawn.jobs.StartJob(job, JobCondition.InterruptForced);
            }

        }

        public void AcquireTarget()
        {
            CurrentTarget = pawn.Map.mapPawns.AllPawnsSpawned
                .InRandomOrder()
                .FirstOrDefault(p => SpreaderVoreTargetUtility.SpreaderCanVoreTarget(p, pawn));

            if(CurrentTarget == null)
            {
                this.RecoverFromState();
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_References.Look(ref CurrentTarget, "CurrentTarget");
        }
    }
}
