﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Verse;
//using Verse.AI;
//using RimVore2;
//using RimWorld;

//namespace CarnivorousPlants
//{
//    public class JobDriver_InitAsPredatorStationary : JobDriver
//    {
//        TargetIndex preyIndex = TargetIndex.A;
//        Pawn Prey => job.GetTarget(TargetIndex.A).Pawn;

//        public override bool TryMakePreToilReservations(bool errorOnFailed)
//        {
//            bool canFatalVore = pawn.CanVore(Prey, out _) && pawn.CanFatalVore(Prey, out _);
//            if (!canFatalVore)
//            {
//                //Log.Message("Can't fatally vore");
//                return false;
//            }
//            return pawn.Reserve(Prey, job, 1, -1, null, errorOnFailed);
//        }

//        protected override IEnumerable<Toil> MakeNewToils()
//        {
//            //Log.Message("can touch? " + pawn.CanReachImmediate(Prey, PathEndMode.Touch));
//            //this.FailOnDespawnedOrNull(preyIndex);
//            //this.FailOnDownedOrDead(preyIndex);
//            //this.FailOnCannotTouch(preyIndex, PathEndMode.Touch);

//            VoreJob voreJob = (VoreJob)job;

//            yield return Toil_Vore.SwallowToil(base.job, pawn, preyIndex);
//            yield return Toil_Vore.ExecutionToil_Direct(voreJob, pawn, pawn, Prey);
//        }
//    }
//}
